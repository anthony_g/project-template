import express from "express";
import config from "./config";
const cors = require("cors");
import "reflect-metadata";
import { createConnection } from "typeorm";
import { User } from "./src/entity/User";

const app = express();
app.use(cors());
app.use(express.json());

(async () => {
  const connection = await createConnection({
    type: "postgres",
    host: config.pgHost,
    port: parseInt(config.pgPort as string),
    username: config.pgUser,
    password: config.pgPassword,
    database: config.pgDatabase,
    entities: [User],
    synchronize: true,
    logging: false
  });

  console.log("Inserting a new user into the database...");
  const user = new User();
  user.firstName = "Timber";
  user.lastName = "Saw";
  user.age = 25;
  await connection.manager.save(user);
  console.log("Saved a new user with id: " + user.id);

  console.log("Loading users from the database...");
  const users = await connection.manager.find(User, {
    where: { firstName: "Timer" }
  });
  console.log("Loaded users: ", users);

  console.log("Here you can setup and run express/koa/any other framework.");
})();

// Redis Client Setup
const redis = require("redis");
const redisClient = redis.createClient({
  host: config.redisHost,
  port: config.redisPort,
  retry_strategy: () => 1000
});

// Express route handlers

app.get("*", (req, res) => {
  res.send("Hi");
});

app.listen(5000, () => console.log("Listening on port 5000"));
