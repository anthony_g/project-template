import express from "express";
import config from "./config";
const cors = require("cors");

const app = express();
app.use(cors());
app.use(express.json());

// Postgres Client Setup
const { Pool } = require("pg");
const pgClient = new Pool({
  user: config.pgUser,
  host: config.pgHost,
  database: config.pgDatabase,
  password: config.pgPassword,
  port: config.pgPort
});
try {
  (async () => {
    const client = await pgClient.connect();
    console.log("Postgress connection success");
  })();
} catch (e) {
  console.log("Postgress connection failed", e);
  process.exit(1);
}

// pgClient
//   .query('CREATE TABLE IF NOT EXISTS values (number INT)')
//   .catch((err:string) => console.log(err));

// Redis Client Setup
const redis = require("redis");
const redisClient = redis.createClient({
  host: config.redisHost,
  port: config.redisPort,
  retry_strategy: () => 1000
});

// Express route handlers

app.get("*", (req, res) => {
  res.send("Hi");
});

app.listen(6000, () => console.log("Listening on port 6000"));
